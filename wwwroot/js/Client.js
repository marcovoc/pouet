﻿
var connection = new signalR.HubConnectionBuilder()
    .withUrl("../pouet/hubPouet")
    .withAutomaticReconnect()
    .build();

var Connected = false;

var audio = new Audio('Sound/pouet.mp3');

connection.onclose(OnDisconnect);

connection.on("Pouet", PlayPouet);

setInterval(KeepAlive, 5000);

function PlayPouet() {
    audio.play();
}

function Connect() {
    connection.start().then(KeepAlive);
    Connected = true;
    $("#Success").text('Connected');
}

function OnDisconnect() {
    Connected = false;
}

function OnReconnect() {
    Connected = true;
}

function KeepAlive() {
    if(Connected)
        connection.invoke("SendKeepAlive", $("#Username").val());
}