﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PouetServer.Models
{
    public class Connection
    {
        public string Username { get; set; }
        public DateTime LastKeepAlive { get; set; }

        public bool IsTimeOut()
        {
            return DateTime.Now - LastKeepAlive > 2 * TimeSpan.FromSeconds(Const.TimeOutSec);
        }

        public bool IsArchived()
        {
            return DateTime.Now - LastKeepAlive > TimeSpan.FromSeconds(Const.ArchivedTimeMinute);
        }
    }
}
