﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PouetServer.Models
{
    public class SingletonData
    {
        private readonly ILogger<SingletonData> _logger;
        private readonly Object _lockUpdate = new object();

        public List<Connection> Connection { get => connections;  }
        private readonly List<Connection> connections = new List<Connection>();
        
        public SingletonData(ILogger<SingletonData> logger)
        {
            _logger = logger;
        }

        public void UpdateUser(string username)
        {
            lock (_lockUpdate)
            {
                var selectedUser = connections.FirstOrDefault(x => x.Username == username);
                if (selectedUser != null)
                {
                    selectedUser.LastKeepAlive = DateTime.Now;
                    _logger.LogInformation($"Keep alive from {username}");
                }
                else
                {
                    _logger.LogInformation($"Connection: new user {username}");
                    connections.Add(new Models.Connection
                    {
                        LastKeepAlive = DateTime.Now,
                        Username = username
                    });
                } 
            }
        }
    }
}
