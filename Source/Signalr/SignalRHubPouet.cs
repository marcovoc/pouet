﻿using Microsoft.AspNetCore.SignalR;
using PouetServer.Models;
using System.Threading.Tasks;

namespace PouetServer.Signalr
{
    public class HubPouet : Hub
    {
        private readonly SingletonData _singletonData;

        public HubPouet(SingletonData singletonData)
        {
            _singletonData = singletonData;
        }

        public Task SendKeepAlive(string username)
        {
            _singletonData.UpdateUser(username);
            return Task.CompletedTask;
        }
    }
}
