﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PouetServer.Signalr
{
    public class PouetHelperServer
    {
        private IHubContext<HubPouet> _hubContext;
        public PouetHelperServer(IHubContext<HubPouet> hubContext)
        {
            _hubContext = hubContext;
        }

        public Task SendAllPouetAsync()
        {
            return _hubContext.Clients.All.SendAsync(Const.HubActionPouet);
        }
    }
}
