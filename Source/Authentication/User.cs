﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PouetServer.Authentication
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public static User FromXmlFile()
        {
            XmlSerializer xs = new XmlSerializer(typeof(User));
            using StreamReader wr = new StreamReader("User.xml");
            return (User)xs.Deserialize(wr);
        }
    }
}
