﻿using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;


namespace PouetServer.Authentication
{
    public class AuthHelper
    {
        public static void SetAuthOption(BasicAuthenticationOptions options)
        {
            options.Realm = "My Application";
            options.Events = new BasicAuthenticationEvents
            {
                OnValidatePrincipal = context =>
                {
                    User user = User.FromXmlFile();
                    if ((context.UserName == user.Username) && (context.Password == user.Password))
                    {
                        var claims = new List<Claim>
                            {
                                new Claim(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer)
                            };

                        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));
                        context.Principal = principal;
                    }

                    return Task.CompletedTask;
                }
            };
        }
    }
}
