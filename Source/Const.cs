﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PouetServer
{
    public class Const
    {
        public const string HubPouetUrl = "hubPouet";
        public const string HubActionPouet = "Pouet";
        public const string HubActionConnect = "Connect";
        public const string HubActionKeepAlive = "KeepAlive";
        public const int TimeOutSec = 5;
        public const int ArchivedTimeMinute = 30;
    }
}
