﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PouetServer.Discord
{
    public class DiscordBackWorker : IHostedService
    {
        private readonly ILogger<DiscordBackWorker> _logger;
        private readonly DiscordSocketClient _client;
        private readonly CommandHandler _commandHandler;
        private Task _mainTask;

        public DiscordBackWorker(ILogger<DiscordBackWorker> logger, DiscordSocketClient client, CommandHandler commandHandler)
        {
            _logger = logger;
            _client = client;
            _commandHandler = commandHandler;
        }

        private async Task MainAsync()
        {
            _client.Log += Log;

            var token = "NjU0NjcwNzk4Mjc5NDA5Njc1.XfNa4Q.s-04bE89SQlgZ26MFd-rkj-YI0Y";

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
            await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            _logger.LogInformation(msg.ToString());
            return Task.CompletedTask;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _mainTask = MainAsync();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
