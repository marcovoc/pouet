﻿using Discord.Commands;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PouetServer.Signalr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PouetServer.Discord
{
	public class BasicModule : ModuleBase<SocketCommandContext>
	{
		private readonly ILogger<BasicModule> _logger;
		private readonly PouetHelperServer _signalrHelper;
		public BasicModule(ILogger<BasicModule> logger, IHubContext<HubPouet> hubContext)
		{
			_logger = logger;
			_signalrHelper = new PouetHelperServer(hubContext);
		}

		[Command("Pouet")]
		[Summary("Play a pouet sound on all client open")]
		public Task PouetAsync()
		{
			_logger.LogInformation("Discord: !Pouet");
			return _signalrHelper.SendAllPouetAsync();
		}

	}
}
