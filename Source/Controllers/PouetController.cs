﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PouetServer.Models;
using PouetServer.Signalr;

namespace PouetServer.Controllers
{
    public class PouetController : Controller
    {
        private readonly ILogger<PouetController> _logger;
        private readonly PouetHelperServer _signalrHelper;
        private readonly SingletonData _singletonData;

        public PouetController(ILogger<PouetController> logger, IHubContext<HubPouet> hubContext, SingletonData singletonData)
        {
            _logger = logger;
            _signalrHelper = new PouetHelperServer(hubContext);
            _singletonData = singletonData;
        }

        [Route("/")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [Route("/ListClient")]
        [HttpGet]
        public IActionResult ListClient()
        {
            return View(_singletonData.Connection);
        }

        [Route("/All")]
        [HttpGet]
        public IActionResult PouetAllClient()
        {
            _signalrHelper.SendAllPouetAsync();
            _logger.LogInformation("PouetAll");
            return Ok();
        }
    }
}
